import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import authService from '@/services/auth'
import { useMessageStore } from './message'
import { useRouter } from 'vue-router'
export const useAuthStore = defineStore('auth', () => {
  const useMessage = useMessageStore()
  const router = useRouter()
  const login = async function (email: string, password: string) {
    try {
      const res = await authService.login(email, password)
      console.log(res.data)
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', JSON.stringify(res.data.access_token))
      router.push('/pos')
    } catch (e: any) {
      console.log(e.message)
      useMessage.showMessage(e.message)
    }
  }
  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser === null) return null
    return JSON.parse(strUser)
  }
  function getToken(): string | null {
    const strToken = localStorage.getItem('access_token')
    if (strToken === null) return null
    return JSON.parse(strToken)
  }

  function logout() {
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
    router.push('/login')
  }
  return { getCurrentUser, login, getToken, logout }
})
